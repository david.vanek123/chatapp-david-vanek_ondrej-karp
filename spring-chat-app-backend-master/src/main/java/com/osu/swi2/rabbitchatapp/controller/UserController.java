package com.osu.swi2.rabbitchatapp.controller;

import com.osu.swi2.rabbitchatapp.model.User;
import com.osu.swi2.rabbitchatapp.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/user")
    @SecurityRequirement(name = "bearerAuth")
    public ResponseEntity<User> getUser(HttpServletRequest request){
        return ResponseEntity.ok(userService.getUserFromRequest(request));
    }
}
