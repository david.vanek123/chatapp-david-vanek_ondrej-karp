package com.osu.swi2.rabbitchatapp.repository;

import com.osu.swi2.rabbitchatapp.model.ChatRoom;
import com.osu.swi2.rabbitchatapp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatRoomRepository extends JpaRepository<ChatRoom, Long> {

    List<ChatRoom> findAllByUserQueues_User(User user);
}
